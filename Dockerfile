FROM php:8.1-fpm

ENV COMPOSER_MEMORY_LIMIT='-1'

# install plugins
RUN apt-get update \
    && apt-get install -y autoconf pkg-config libssl-dev libpng-dev libzip-dev libjpeg-dev libfreetype6-dev

# Install PHP extensions
RUN docker-php-ext-install bcmath
RUN docker-php-ext-install pdo
RUN docker-php-ext-install pdo_mysql
RUN docker-php-ext-configure gd --with-jpeg --with-freetype
RUN docker-php-ext-install gd
RUN docker-php-ext-enable gd
RUN docker-php-ext-install exif
RUN docker-php-ext-install pcntl
RUN docker-php-ext-install bcmath
RUN docker-php-ext-install zip

# Install the PHP zip extention
RUN docker-php-ext-install zip

# INSTALL SUPERVISORD
RUN apt-get update \
    && apt-get install -y nginx supervisor cron

# INSTALL COMPOSER
RUN curl -sS https://getcomposer.org/installer | php && mv composer.phar /usr/local/bin/composer


COPY docker/php/php.ini /usr/local/etc/php/php.ini

# RUN docker-php-ext-install opcache
# COPY opcache.ini /usr/local/etc/php/conf.d/opcache.ini

RUN pecl install xdebug 
RUN docker-php-ext-enable xdebug
ADD xdebug.ini /usr/local/etc/php/conf.d/xdebug.ini    


RUN apt-get update && apt-get install -y \
    software-properties-common \
    npm
RUN npm install npm@latest -g && \
    npm install n -g && \
    n latest
    
RUN chown -R www-data:www-data /var/www
WORKDIR /var/www/html

# open port 80 443
EXPOSE 80 443

COPY docker/supervisord/supervisord.conf /etc/supervisor/supervisord.conf

#CMD cd /var/www/html && composer install
# run supervisord
CMD /usr/bin/supervisord -n -c /etc/supervisor/supervisord.conf
