<?php

namespace App\Http\Requests;

use App\Supports\Common;
use App\Utils\HttpStatusCode;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function failedValidation(Validator $validator) {
        
        return Common::exception($validator->errors(), HttpStatusCode::BAD_REQUEST); 

    }
    
    public function rules()
    {
        $method = $this->method();

        $action = $this->route()->getActionMethod();

        switch ($method) {
            case "POST":
                if($action == 'login'){
                    $rules = [
                        'email'    => ['required', 'exists:user,email'],
                        'password' => [
                            'required', $this->loginStatus()
                        ],
                    ];
                }elseif($action == 'refreshToken'){
                    $rules = [
                        'refresh_token'    => ['required'],
                       
                    ];
                }
                
                break;

            default:

            return [];
        }

        return $rules ;
    }

    public function loginStatus(){

        return function ($attribute, $value, $fail) {
            $inputs = array(
                'email' => $this->email,
                'password' => $this->password
            );

            if (!auth()->attempt($inputs)) {
                $fail('Email or password not correct.');
            }
        };

    }

    public function messages()
    {
        $messages = [
            'required' => __('validation.required'),
            'email' => __('validation.email'),
        ];

        return $messages ;
    }
}
