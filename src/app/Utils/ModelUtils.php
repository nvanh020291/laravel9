<?php

namespace App\Utils;

use Illuminate\Support\Facades\DB;

class ModelUtils
{

    const SORT_ORDER_PREFIX = '-';
    const SPACE             = ' ';

    protected $mapping_fields = [];
    protected $tableName = '';

    public function __construct($mapping_fields = [], $tableName = '')
    {
        $this->mapping_fields = $mapping_fields;
        $this->tableName = $tableName;
    }

    /**
     *
     * For standard Operators. The convention is to follow what's specified at
     *
     * https://github.com/typicode/json-server#operators
     *
     * For Sorting support:
     *
     * Add order_by (ascending order by default if none specified)
     *
     * GET /posts?order_by=views-asc
     * GET /posts/1/comments?order_by=votes-asc
     *
     * For multiple fields, use the following format (Relate to how you do it with SQL for easy memorization)
     *
     * GET /posts?order_by=email-desc,full_name,id-asc
     *
     *
     */
    const OPERATORS = [
        '_eq'   => '=',
        '_ne'   => '<>',
        '_gte'  => '>=',
        '_gt'   => '>',
        '_lte'  => '<=',
        '_lt'   => '<',
        '_like' => '%',
        '_ina'  => 'IN',
        '_in'   => 'IN',
        '_nin'  => 'NOT IN',
        '_nul'  => 'IS NULL',
        '_nnul' => 'IS NOT NULL',
        '_oreq' => '=',
        '_btw'  => 'BETWEEN',
        '_has'  => 'HAS|EXISTS',
    ];

    protected static function parseOperators($params)
    {
        $criteria = [];

        if (count($params)) {
            foreach ($params as $key => $val) {
                if (!is_array($val) && strlen(trim($val)) === 0) {
                    continue;
                }

                # TODO: should use filter to filter data
                if (is_array($val)) {
                    $tmp = [];
                    foreach ($val as $v) {
                        $tmp[] = trim($v);
                    }
                    $val = $tmp;
                } else {
                    $val = trim($val);
                }

                # sample https://regex101.com/r/e0rhJZ/2
                if (preg_match("/^([a-zA-Z0-9>_\.]+)(" . implode('|', array_keys(self::OPERATORS)) . ")$/", $key, $matches)) {
                    $field = str_replace('>', '.', $matches[1]);
                    $operator = $matches[2];
                    $criteria[] = [$field, $operator, $val];
                }
            }
        }

        return $criteria;
    }

    public function getInclude()
    {
        $include = request()->input('include', '');
        if (empty($include)) {
            return collect([]);
        }
        return collect(explode(',', $include));
    }

    public function withInclude($model)
    {
        $this->getInclude()->each(function ($include) use (&$model) {
            $include = explode(':', $include);
            $includeField = $include[0] . '_include';

            if (isset($this->mapping_fields[$includeField])) {
                $model = $this->mapping_fields[$includeField]($model, $include);
            } else if (isset($include[1]) && $include[1] == 'count') {
                $model = $model->withCount($include[0]);
            } else {
                $model = $model->with($include[0]);
            }
        });
        return $model;
    }

    public function loadInclude($model)
    {
        $includes = [];
        $this->getInclude()->each(function ($include) use (&$model, &$includes) {
            $include = explode(':', $include);
            $includeField = $include[0] . '_include';

            if (isset($this->mapping_fields[$includeField])) {
                $model = $this->mapping_fields[$includeField]($model, $include);
            } else if (isset($include[1]) && $include[1] == 'count') {
                $model = $model->loadCount($include[0]);
            } else {
                $includes[] = $include[0];
            }
        });
        if (!empty($includes)) {
            $model = $model->load($includes);
        }
        return $model;
    }

    public function search($model, $options = [], $extra_params = [])
    {
        $model = empty($options['ignore_include']) ? $this->withInclude($model) : $model;
        $params = request()->input();

        $criteria = $this->parseOperators(array_merge($params, $extra_params));
        $this->setConditions($model, $criteria, false);

        $this->orderBy($model, $options);

        $isPagination = $options['is_pagination'] ?? true;
        if ($isPagination) {
            return $model->paginate(request()->input('per_page', 10));
        }

        return $model->get();
    }

    private function setConditions(&$model, $criteria)
    {
        foreach ($criteria as $idx => $q) {
            $field = $q[0];
            $operator = $q[1];
            $value = $q[2];
            if (isset($this->mapping_fields[$field . $operator])) {
                if (!empty($value) || $value == '0') {
                    $model = $this->mapping_fields[$field . $operator]($model, $value);
                }
            } else {
                $field = !empty($this->tableName) && !str_contains($field, '.') ? $this->tableName . '.' . $field : $field;
                switch ($operator) {
                    case '_lte':
                    case '_lt':
                    case '_gte':
                    case '_gt':
                    case '_ne':
                    case '_eq':
                        $sqlOperator = self::OPERATORS[$operator];
                        $model = $model->where($field, $sqlOperator, $value);
                        break;
                    case '_nul':
                        $model = ($value == '1' ? $model->whereNull($field) : $model->whereNotNull($field));
                        break;
                    case '_nnul':
                        $model = ($value == '1' ? $model->whereNotNull($field) : $model->whereNull($field));
                        break;
                    case '_like':
                        # TODO: escape array values to prevent sql injection
                        $model = $model->where($field, 'like', "%$value%");
                        break;
                    case '_in':
                    case '_ina':
                        $value = is_array($value) ? $value : explode(',', $value);
                        $model = $model->whereIn($field, $value);
                        break;
                    case '_nin':
                        $value = is_array($value) && count($value) ? $value : explode(',', $value);
                        $model = $model->whereNotIn($field, $value);
                        break;
                    case '_btw':
                        $value = is_array($value) && count($value) > 1 ? $value : explode(',', $value);
                        $model = $model->whereBetween($field, $value);
                        break;
                    case '_has':
                        $method = ($value == '1' ? 'whereHas' : 'whereDoesntHave');
                        $model = $model->$method($field);
                        break;
                }
            }
        }
    }

    private function orderBy(&$model, $options = [])
    {
        // In Query
        $orderBy = request()->input('order_by', '');
        $orderBy = !empty($orderBy) ? explode(',', $orderBy) : [];

        // In Optional
        $orderBy = array_merge($orderBy, !empty($options['order_by']) ? explode(',', $options['order_by']) : []);
        if (!empty($orderBy)) {
            foreach ($orderBy as $order) {
                list($orderByName, $direction) = self::splitOrderBy($order);
                if (str_ends_with($orderByName, '.by_join')) {
                    $orderByName = substr($orderByName, 0, -8);
                    $model = $model->orderByLeftPowerJoins($orderByName, $direction);
                } else {
                    $model = $model->orderBy($orderByName, $direction);
                }
            }
        }
    }

    public static function splitOrderBy($order)
    {
        $order = preg_split("/[\s-]/", $order);
        $orderByName = $order[0];
        $direction = $order[1] ?? 'asc';
        return array($orderByName, $direction);
    }
}
