<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use App\Models\Image;
use App\Models\Comment;
use App\Supports\Common;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postDetail($id)
    {

        $posts = Post::withExists('comments')->get();

        $comments = Comment::with(['commentable' => function (MorphTo $morphTo) {
            $morphTo->constrain([
                Post::class => function (Builder $query) {
                    $query->where('title','This is title post2');
                },
            ]);
        }])->get();

       
        $res =  Post::with('comments')->find($id);

        return Common::responseSuccess($comments);
    }

    public function updatePost(Request $request ,$id)
    {

        $user = Post::updateOrCreate(
            ['title' =>  request('title')],
            ['body' => request('body')]
        );


        $user = Post::firstOrCreate(
            ['title' =>  request('title') ,'body' => request('body')],
            ['body' => request('body')]
        );


        dd($user);
        
        $attributes  = $request->all();

        $post = Post::find($id);

        $post->update(['title' => $attributes['title'] , 'body' => $attributes['body']]);

        //$post->comments()->delete();

        //Comment::whereNotIn('id' , $commentIds)->where('commentable_id' , $id)->delete();

        foreach($attributes['comments'] as $data){
            if(!empty($data['id'])){

                $commentIds[] = $data['id'] ;

                Comment::where('id', $data['id'])->update($data);

            }else{

                $post->comments()->create($data);
            }
        }


        return Common::responseSuccess(Post::with('comments')->find($id));
    }


    public function addPost()
    {
        $attributes  =  array(
            'title' => 'This is title post',
            'body' => 'This is title post',
            'comments' => array(
                'first comment' , 'second comment'
            )
        );

        $listComment = array();

        if(!empty($attributes['comments'])){
            foreach($attributes['comments'] as $comment){
                $listComment[]['body'] = $comment ; 
            }
        }

        $attributes['comments'] =  $listComment ; 

        $post = Post::create(['title' => $attributes['title'] , 'body' => $attributes['body']]);

        $post->comments()->createMany($attributes['comments']);

        return Common::responseSuccess($post);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = $request->all();

        $user = User::create(['name' => $attributes['name']]);

        $user->roles()->attach($attributes['roles']);

        return Common::responseSuccess($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $res =  User::with('roles')->find($id);

        return Common::responseSuccess($res);
    }
    public function update(Request $request, $id)
    {
        $attributes  = $request->all();

        $user = User::find($id);

        $user->update(['name' => $attributes['name']]);

        $user->roles()->syncWithoutDetaching($attributes['roles']);

        return Common::responseSuccess($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
