<?php

namespace App\Http\Controllers;

use Beste\Json;
use App\Supports\Common;
use Illuminate\Http\Request;
use App\Utils\HttpStatusCode;
use App\Repositories\UserEloquentRepository;
use Kreait\Laravel\Firebase\Facades\Firebase;
use Kreait\Firebase\Exception\Auth\FailedToVerifyToken;

class FirebaseAuthController extends Controller
{
    protected $auth;
    protected $userEloquentRepository;

    public function __construct(UserEloquentRepository $userEloquentRepository)
    {
        $this->auth = Firebase::auth();
        $this->userEloquentRepository = $userEloquentRepository;
    }

    public function login(Request $request)
    {
        $input  = $request->all();

        try {
           // dd($input['id_token']);
            $verifiedIdToken = $this->auth->verifyIdToken($input['id_token']);

            $uid = $verifiedIdToken->claims()->get('sub');

            $user = $this->auth->getUser($uid);
           
            if(!empty($user->providerData[0])){
    
                $data  =  $this->userEloquentRepository->loginFirebase($user->providerData[0]);

                return Common::responseSuccess($data);
    
            }else{
                return Common::responseErrors('Token invalid', HttpStatusCode::BAD_REQUEST);
            }
    
           
        } catch (FailedToVerifyToken $e) {
            echo 'The token is invalid: ' . $e->getMessage();
        }

    }
}
