<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Comment extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'body'
    ];

    protected $hidden = [
        'commentable_id','commentable_type'
    ];

    public function commentable(): MorphTo
    {
        return $this->morphTo();
    }
}
