<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WebNotificationController;
use Illuminate\Support\Facades\Auth;


Route::get('/', function () {
    return view('welcome');
});


Route::get('/success', function () {
    return view('success');
});