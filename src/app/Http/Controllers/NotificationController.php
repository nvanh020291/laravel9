<?php

namespace App\Http\Controllers;

use App\Traits\PushNotificationTrait;


class NotificationController extends Controller
{

    use PushNotificationTrait;

    public function sendPush()
    {
       
           $deviceToken =  'fylvRe-C1YJtAMcXTgKC03:APA91bFro4y50jT4DRGXptFI1RTrMMtqpPkndPpqKDu-aNxgnalBvfgpkPZiqh1rQZJ-oCsKgEi4EahsyKF0h_Ir2bE8azKJK2b8YlGj_p8TTZ8roUH_F42pNpYlBHtzpERU7RMU43Is' ;
           
           $totalUnread = 1;
           $data = [
                'func_name' => config('firebase.notification.func'),
                'screen' => config('firebase.notification.screen'),
                'total_unread' => $totalUnread,
                'total_count' => 2,
                'device_type' => 'web', // Loại device, có thể là androi, web, ios
            ];
            $content = [
                "title" => "hello", // tiêu đề tin nhắn
                "body" => "test", // nội dung tin nhắn
                'badge' => $totalUnread, // số message chưa đọc
                'sound' => config('firebase.sound') // âm báo tin nhắn
            ];

            

            // Push notification
            $this->pushMessage($deviceToken, $content, $data);

            return view('success');
    }
}