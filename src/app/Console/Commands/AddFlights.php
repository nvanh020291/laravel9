<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AddFlights extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'AddFlights';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'add flights every minute';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::channel('cron')->info('start!');

        DB::table('flights')->insert([
            'name' => 'kayla@example.com',
            'airline' => 'kayla@example.com',
        ]);

        Log::channel('cron')->info('End!');
    }
}
