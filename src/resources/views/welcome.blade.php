
  <!-- Insert this script at the bottom of the HTML, but before you use any Firebase services -->
  <script type="module">
    import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.19.1/firebase-app.js'

    import { getMessaging, getToken,  onMessage} from "https://www.gstatic.com/firebasejs/9.19.1/firebase-messaging.js";
    import { onBackgroundMessage} from "https://www.gstatic.com/firebasejs/9.19.1/firebase-messaging-sw.js";

    const firebaseConfig = {
      apiKey: "AIzaSyA6ctbqK-GFTa84ZVdr5oc2hX-5UB_I5zw",
      authDomain: "manifest-emblem-378009.firebaseapp.com",
      databaseURL: "https://manifest-emblem-378009-default-rtdb.asia-southeast1.firebasedatabase.app",
      projectId: "manifest-emblem-378009",
      storageBucket: "manifest-emblem-378009.appspot.com",
      messagingSenderId: "77947156276",
      appId: "1:77947156276:web:1d13279344dd385ead1761",
      measurementId: "G-2PCC4HKBM3"
    };
    // Initialize Firebase
    const app = initializeApp(firebaseConfig);

    const messaging = getMessaging();

    getToken(messaging, { vapidKey: 'BMsx13O446OYVYd2Kn6q4UqJWO2qsqkjSjLnkKHExFwozqjg97egLt5GQOJ2rPTKHujgnP-zBhxDtAFPxGv4zW0' }).then((currentToken) => {
    if (currentToken) {
        console.log(currentToken);
    } else {
        // Show permission request UI
        console.log('No registration token available. Request permission to generate one.');
        // ...
    }
    }).catch((err) => {
    console.log('An error occurred while retrieving token. ', err);
    // ...
    });


  onMessage(messaging, (payload) => {
    console.log('Message received. ', payload);
    // ...
  });



  </script>

  