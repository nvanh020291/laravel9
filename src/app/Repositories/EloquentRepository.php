<?php

namespace App\Repositories;

use Closure;

abstract class EloquentRepository implements RepositoryInterface{

    protected $model;

    /**
     * EloquentRepository constructor.
     */
    public function __construct(){
        $this->model = $this->setModel();
    }

    abstract function getModel();

    public function setModel(){
        return app()->make($this->getModel());
    }

    public function all($columns = array('*'))
    {
        // TODO: Implement all() method.

        return $this->model->get($columns);
    }

    public function create(array $attr)
    {
        // TODO: Implement create() method.
        $data = $this->model->newInstance($attr);
        $data->save();
        return $data;
    }

    public function update($id, array $attr)
    {
        // TODO: Implement update() method.

        $result = $this->model->find($id);
        if ($result){
            $result->fill($attr);
            $result->save();
            return $result;
        }

        return false;
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.

        $result = $this->model->find($id);

        if ($result){
            $result->delete();
            return true;
        }

        return false;
    }

    public function find($id, $columns = array('*'))
    {
        // TODO: Implement find() method.
        $result = $this->model->find($id , $columns);

        if ($result){
            return $result;
        }

        return false;
    }

    public function first($columns = array('*'))
    {
        // TODO: Implement first() method.

        return $this->model->first($columns);
    }

    public function findByField($field, $value, $columns = array('*'))
    {
        // TODO: Implement findByField() method.

        return $this->model->where($field, $value)->get($columns);
    }

    public function findWhere(array $where, $columns = array('*'))
    {
        // TODO: Implement findWhere() method.

        $this->applyConditions($where);

        return $this->model->get($columns);
    }

    public function findWhereIn($field, array $where, $columns = array('*'))
    {
        // TODO: Implement findWhereIn() method.

        return $this->model->whereIn($field, $where)->get($columns);
    }

    public function findWhereNotIn($field, array $where, $columns = array('*'))
    {
        // TODO: Implement findWhereNotIn() method.

        return $this->model->whereNotIn($field, $where)->get($columns);
    }

    public function searchable($field, $value, $like = false)
    {
        // TODO: Implement searchable() method.
        if ($like){
            return $this->model->where($field , 'like' , "%$value%");
        }

        return $this->model->where($field, $value);
    }

    public function has(string $relation)
    {
        // TODO: Implement has() method.
        $this->model = $this->model->has($relation);
        return $this;
    }

    public function whereHas(string $relation, closure $closure)
    {
        // TODO: Implement whereHas() method.
        $this->model = $this->model->whereHas($relation , $closure);
        return $this;
    }

    public function with(string $relations)
    {
        // TODO: Implement with() method.
        $this->model = $this->model->with($relations);
        return $this;
    }

    public function withCount($relations)
    {
        // TODO: Implement withCount() method.
        $this->model = $this->model->withCount($relations);
        return $this;
    }

    public function orderBy($columns, $direction = 'asc')
    {
        // TODO: Implement orderBy() method.
        $this->model = $this->model->orderBy($columns ,$direction );
        return $this;
    }

    public function paginate($limit, $columns = array('*'))
    {

        // TODO: Implement paginate() method.

        $limit = is_null($limit) ? 15 : $limit;

        return $this->model->paginate($limit , $columns);
    }


    protected function applyConditions(array $where){
        foreach ($where as $field => $value){
            $this->model = $this->model->where($field, $value);
        }
    }

}
