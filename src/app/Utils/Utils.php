<?php

namespace App\Utils;

use Illuminate\Support\Facades\DB;

class Utils
{
    /*
     * Get total char '?' by array params
     */
    public static function whereIn($ids)
    {
        $sql = [];
        foreach ($ids as $id) {
            $sql[] = '?';
        }
        return implode(',', $sql);
    }

    /*
     * Parse DB select to Array
     */
    public static function raw($sql, $params)
    {
        $result = DB::select($sql, $params);
        return array_map(function ($value) {
            return (array)$value;
        }, $result);
    }

    public static function dumpSql($model)
    {
        $replace = function ($sql, $bindings) {
            $needle = '?';
            foreach ($bindings as $replace) {
                $pos = strpos($sql, $needle);
                if ($pos !== false) {
                    if (gettype($replace) === "string") {
                        $replace = ' "' . addslashes($replace) . '" ';
                    }
                    $sql = substr_replace($sql, $replace, $pos, strlen($needle));
                }
            }
            return $sql;
        };
        $sql = $replace($model->toSql(), $model->getBindings());

        return $sql;
    }
}
