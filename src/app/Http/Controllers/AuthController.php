<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Supports\Common;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Laravel\Socialite\Facades\Socialite;
use App\Repositories\UserEloquentRepository;

class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $userEloquentRepository;

    public function __construct(UserEloquentRepository $userEloquentRepository)
    {
        $this->userEloquentRepository = $userEloquentRepository;
    }

    public function register(Request $request)
    {

        $attributes  = $request->all();

        $attributes['password'] = bcrypt($request->password);

        $data  =  $this->userEloquentRepository->create($attributes);

        return Common::responseSuccess($data);
    }

    public function refreshToken(UserRequest $request)
    {

        $attributes = $request->validated();

        $data  =  $this->userEloquentRepository->refreshToken($attributes['refresh_token']);

        return Common::responseSuccess($data);
    }

    public function socialLogin(Request $request)
    {

        // LOGIN WITH GOOGLE
        $tokengoogle = 'ya29.a0AVvZVspLwL8N5Ty1DJfoa1vG-VN3tL8-Z01XJn2BGxYJ1l3KCmjR-38N-eegY911VVc74lh9zKoswOhx4Y988T1K4wYJEbNLTidf85UR0F0HUP4Y--kglHXdUbQM5z-LolpdL9ViLzkqQxjZLZMlzs4ymKUNaCgYKAakSARESFQGbdwaI2FWdaVM_3blGtJ6XpRzA3g0163';


        $user = Socialite::driver('google')->userFromToken($tokengoogle);


        dd($user);

        // LOGIN WITH FACEBOOK
        $tokenfacebook = 'EAAHtCnuDrZAgBALtXOGu9syXyqlPR7pKlwOgrWyXehH0OnTYEb1MPuige7Cx3Ui0DeZCNLWxnoEmHC4EyTUnOat1beagIaYMrqkWU7sFsXXkn0EAFR0FwwYQVTZAh0BH9HU1ByaR4FmDMAxlc7WyMlR1VDlZBUqXgvrP5o1u7Wb5uzE71XQB9uWqv8Aa1ZC7rVjgtOEg9JaRVw0BF5ZCAg';

        $user = Socialite::driver('facebook')->userFromToken($tokenfacebook);

        dd($user);

        $attributes = $request->all();

        $data  =  $this->userEloquentRepository->grantSocialToken($attributes);

        return Common::responseSuccess($data);
    }

    public function login(UserRequest $request)
    {

        $attributes = $request->validated();

        $data  =  $this->userEloquentRepository->grantPasswordToken($attributes);

        return Common::responseSuccess($data);
    }

    public function logout(Request $request)
    {

        $data  =  $this->userEloquentRepository->logout();

        return Common::responseSuccess($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
