<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $table = 'users';

    protected $fillable = [
        'name','email','password','device_key'
    ];

    public $timestamps = false;

    public function image(): MorphOne
    {
        return $this->morphOne(Image::class, 'imageable');
    }
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class)->wherePivot('active' ,  0)->withPivot('active');
    }
}
