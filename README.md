# hideme-backend

# Setup
- Php 8.1
- Mysql 8.x
- Composer 2
- Laravel 9.x

# Initialize Project Commands
- Create database schema `hide_me`
- Get packages by composer command `composer i`.
- Run `chmod 777 -R src/storage/` to set permission for storage folder
- Copy env file from `.env.example` to `.env`.
- Migrate database: `php artisan migrate`.
- Install passport: `php artisan passport:install`
- Generate oauth key for passport: `php artisan passport:keys`
- Run `php artisan storage:link` to create the link between storage & public folder. It's defined in `filesystem.php`