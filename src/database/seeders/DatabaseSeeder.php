<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Flight;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //\App\Models\User::factory(10)->create();
        if (App::environment('local')){
            // The environment is local
            $this->truncateData();
        }
        $this->insertFlight(); 
    }

    public function fakeData(){
        $this->insertFlight();
    }

    public function truncateData(){
        Flight::truncate();
    }

    public function insertFlight(){
        $i = 1 ;
        while($i < 10){
            Flight::insert([
                'name' => Str::random(10),
                'airline' => Str::random(10).'@gmail.com'
            ]);
            $i ++;
        }  
    }
}
