<?php

namespace App\Supports;

use Carbon\Carbon;
use App\Utils\HttpStatusCode;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

class Common {

    public static function responseErrors($message , $code){
        return response()->json(['message' => $message] , $code);
    }

    public static function responseSuccess($data , $code = HttpStatusCode::OK){

        return response()->json($data ?? null , $code);
    }

    public static function exception($msg, $code = JsonResponse::HTTP_BAD_REQUEST)
    {
        throw new HttpResponseException(response()->json(['message' => $msg], $code));
    }

    public static function logFile($data, $fileName = 'system')
    {
        $timeNow = Carbon::now('Asia/Ho_Chi_Minh');

        $timeDetail = $timeNow->format(FORMAT_DATETIME_SAVE_DB);

        $logName = $fileName . '_' . $timeNow->format(FORMAT_DATE_SAVE_DB) . '.txt';

        $data = is_string($data) ? $data : json_encode($data);

        file_put_contents(storage_path('logs/' . $logName), $timeDetail . ' : ' . $data . "\n", FILE_APPEND);
    }

    public static function charCodeAt($keyword){
        list(, $ord) = unpack('N', mb_convert_encoding($keyword, 'UCS-4BE', 'UTF-8'));
        return  $ord;
    }


    public static function getStringWhereRawLikeCharacterKorean($column, $keyword){

        $where_str = null;

        if($column and $keyword){

            if (self::charCodeAt($keyword) <= 12622) {
                switch (self::charCodeAt($keyword)) {

                    case 12593:
                        $where_str = "(${column} >= '가' and ${column}  <= '깋')";
                        break;

                    case 12594:
                        $where_str = "(${column}  >= '까' and ${column}  < '나' )";
                        break;

                    case 12596:
                        $where_str = "(${column}  >= '나' and ${column}  < '다' )";
                        break;

                    case 12599:
                        $where_str = "(${column}  >= '다' and ${column}  <= '딯' )";
                        break;

                    case 12600:
                        $where_str = "(${column}  >= '따' and ${column}  < '라' )";
                        break;

                    case 12601:
                        $where_str = "(${column}  >= '라' and ${column}  < '마' )";
                        break;

                    case 12609:
                        $where_str = "(${column}  >= '마' and ${column}  < '바' )";
                        break;

                    case 12610:
                        $where_str = "(${column}  >= '바' and ${column}  <= '빟' )";
                        break;

                    case 12611:
                        $where_str = "(${column}  >= '빠' and ${column}  < '사' )";
                        break;

                    case 12613:
                        $where_str = "(${column}  >= '사' and ${column}  <= '싷' )";
                        break;

                    case 12614:
                        $where_str = "(${column}  >= '싸' and ${column}  < '아' )";
                        break;

                    case 12615:
                        $where_str = "(${column}  >= '아' and ${column}  < '자' )";
                        break;

                    case 12616:
                        $where_str = "(${column}  >= '자' and ${column}  <= '짛' )";
                        break;

                    case 12617:
                        $where_str = "(${column}  >= '짜' and ${column}  < '차' )";
                        break;

                    case 12618:
                        $where_str = "(${column}  >= '차' and ${column}  < '카' )";
                        break;

                    case 12619:
                        $where_str = "(${column}  >= '카' and ${column}  < '타' )";
                        break;

                    case 12620:
                        $where_str = "(${column}  >= '타' and ${column}  < '파' )";
                        break;

                    case 12621:
                        $where_str = "(${column}  >= '파' and ${column}  < '하' )";
                        break;

                    case 12622:
                        $where_str = "(${column}  >= '하' and ${column}  <= '힣' )";
                        break;
                    default:
                        $keyword = str_replace("_", "\\_", $keyword);
                        $keyword = str_replace("'", "\\'", $keyword);
                        $keyword = str_replace("%", "\\%", $keyword);
                        $where_str = "(${column} like '%${keyword}%')";
                        break;
                }
            } else {
                $keyword = str_replace("_", "\\_", $keyword);
                $keyword = str_replace("'", "\\'", $keyword);
                $keyword = str_replace("%", "\\%", $keyword);
                $where_str = "(${column} like '%${keyword}%')";
            }
        }
        return $where_str;
    }

}
