<?php

define('ACTIVE' , 1);
define('FORMAT_DATETIME_SAVE_DB' , 'Y-m-d H:i:s');
define('FORMAT_TIME_SAVE_DB' , 'H:i:s');
define('FORMAT_DATE_SAVE_DB' , 'Y-m-d');
define('TOKEN_STANDARD' , ['ERC-721','ERC-1155']);
define('REGULAR_REWARD' , 1);
define('CONDITION_REWARD' , 2);
define('MISSION_TYPE' , [1,2]);
define('MAX_MISSION' , 3);
define('BANNER_STATUS' , [1,2]);
define('MAX_ITEM_NFT_OPENSEA_PER_REQUEST' , 200);
