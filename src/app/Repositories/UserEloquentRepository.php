<?php
namespace App\Repositories;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class UserEloquentRepository extends EloquentRepository {

    function getModel()
    {
        return User::class;
    }
    
    public function oauthClients()
    {
         return DB::table('oauth_clients')->where('password_client', 1)->first();
    } 

    public function register($attributes){

        return $this->create($attributes);
        
    }

    public function createToken($data = array()){

        $oauthClients =  $this->oauthClients();

        $auth = array(
            'client_id' => $oauthClients->id,
            'client_secret' => $oauthClients->secret,
        );

        $request = Request::create('/oauth/token', 'POST', array_merge($data, $auth));

        return json_decode(app()->handle($request)->getContent());
    }

    public function grantPasswordToken($attributes){

        $data =  array(
            'grant_type' => 'password',
            'username' => $attributes['email'],
            'password' => $attributes['password'],
            'scope' => '',
        );

        return $this->createToken($data);
    }

    public function logout(){
        if (Auth::check()) {
            Auth::user()->token()->revoke();    
        } 
        return true ; 
    }

    public function personalToken($user){

        $accessToken = $user->createToken('personal token')->accessToken;

        return array('access_token' => $accessToken);

    }

    public function loginFirebase($attributes){

        $user   = User::where('social_id',$attributes->uid)->first();

        if(empty($user)){

            $data = array(
                'name' => $attributes->displayName ?? '',
                'email' => $attributes->email ?? '',
                'social_id' => $attributes->uid ?? '',
                'password' => $attributes->password ?? '',
                'provider' => $attributes->providerId ?? ''
            );
        
            $matchThese = ['social_id'=> $attributes->uid ?? ''];
    
            $user = User::updateOrCreate($matchThese,$data);
        }

        //return $this->personalToken($user);

        // custom grant type token 
        return $this->grantSocialToken($attributes->uid);
    }

    public function grantSocialToken($social_id){

        $data =  array(
            'grant_type' => 'social_grant',
            'social_id' => $social_id,
            'provider' => 'users',
            'scope' => '',
        );

        return $this->createToken($data);
    }

    public function refreshToken($refresh_token){

        $data =  array(
            'grant_type' => 'refresh_token',
            'refresh_token' => $refresh_token,
            'scope' => ''
        );

        return $this->createToken($data);
    }
}
