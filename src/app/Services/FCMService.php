<?php

namespace App\Services;

use GuzzleHttp\Client;
use Log;

class FCMService
{

    private $apiConfig;

    public function __construct()
    {
        //dd(config('firebase'));

        $this->apiConfig = [
            'url' => config('firebase.push_url'),
            'server_key' => config('firebase.server_key'),
            'device_type' => config('firebase.device_type')
        ];
    }

    /**
     * Sending push message to single user by Firebase
     *
     * @param string $device_token
     * @param array $notification
     * @param array $data
     *
     * @return bool|string
     */
    
    public function send(string $device_token, array $notification, array $data)
    {
        

        if ($data['device_type'] === $this->apiConfig['device_type']['ios']) {
            $fields = [
                'to'   => $device_token,
                'notification' => $notification,
                'data' => $data
            ];
        } else {
            $fields = [
                'to'   => $device_token,
                'data' => array_merge($data, $notification)
            ];
        }

        return $this->sendPushNotification($fields);
    }

    /**
     * Sending push message to multiple users by firebase
     * @param array $device_tokens
     * @param array $notification
     * @param array $data
     *
     * @return bool|string
     */
    public function sendMultiple(array $device_tokens, array $notification, array $data)
    {
        $fields = [
            'registration_ids' => $device_tokens,
            'data' => $data,
            'notification' => $notification
        ];

        return $this->sendPushNotification($fields);
    }

    /**
     * GuzzleHTTP request to firebase servers
     * @param array $fields
     *
     * @return bool
     */
    private function sendPushNotification(array $fields)
    {
        $SERVER_API_KEY = $this->apiConfig['server_key'];
  
        $data = [
            "registration_ids" => ['dIdE-Gpaei-s7MH7xrf8ui:APA91bE8-h8L6NSGtY3Zu4VFItui_NfFU3FIAiIqUDIuuzht0uIoxvKlivIn9m6HgX9FBaaxag8z6Nw_kAviaBFkfK-X5GfEK7_4-gr0Lm3w5SfY5dPevElYUu3AIYTIhM2haZFLnHoT'],
            "notification" => [
                "title" => 'test',
                "body" => 'tasdsest',  
            ]
        ];
        $dataString = json_encode($data);
    
        $headers = [
            'Authorization: key=' . $SERVER_API_KEY,
            'Content-Type: application/json',
        ];
    
        $ch = curl_init();
      
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
               
        $response = curl_exec($ch);
  
        dd($response);

        $client = new Client([
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'key='. $this->apiConfig['server_key'],
            ]
        ]);
        $res = $client->post(
            $this->apiConfig['url'],
            ['body' => json_encode($fields)]
        );
        // Request api fcm by json data
        // $res = $client->post($this->apiConfig['url'], json_encode($fields), ['type' => 'json']);

        $res = json_decode($res->getBody());

        dd($res);
    
        if ($res->failure) {
            Log::error("ERROR_PUSH_NOTIFICATION: ".$fields['to']);
        }

        return true;
    }
} 