<?php

namespace App\Repositories;

use Closure;

interface RepositoryInterface {

    public function find($id , $columns = array('*'));

    public function create(array $attr);

    public function update($id , array $attr);

    public function delete($id);

    public function all($columns = array('*'));

    public function first($columns = array('*'));

    public function findByField($field , $value , $columns = array('*'));

    public function findWhere(array $where , $columns = array('*'));

    public function findWhereIn($field , array $where , $columns = array('*'));

    public function findWhereNotIn($field , array $where , $columns = array('*'));

    public function orderBy($columns , $direction = 'asc');

    public function with(string $relations);

    public function has(string $relation);

    public function whereHas(string $relation , closure $closure);

    public function searchable($field, $value, $like = false);

    public function withCount($relations);

    public function paginate($limit , $columns = array('*'));


}
