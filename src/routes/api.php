<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\FirebaseAuthController;
use App\Http\Controllers\NotificationController;

Route::group([
    'prefix' => 'auth'
    ], function ($router) {

    Route::post('login', [AuthController::class, 'login']);
    Route::post('register', [AuthController::class, 'register']); 
    Route::post('social-login', [AuthController::class, 'socialLogin']); 
    Route::post('refresh-token', [AuthController::class, 'refreshToken']); 
    Route::post('firebase-login', [FirebaseAuthController::class, 'login']);
 
});


Route::get('push', [NotificationController::class, 'sendPush']);

Route::middleware(['auth:api'])->group(function () {

    Route::post('post', [UserController::class, 'addPost']);
    Route::get('post/{id}', [UserController::class, 'postDetail']);
    Route::put('post/{id}', [UserController::class, 'updatePost']);

    Route::post('user', [UserController::class, 'store']);
    Route::get('user/{id}', [UserController::class, 'show']);
    Route::put('user/{id}', [UserController::class, 'update']);

    //Route::get('user', [UserController::class, 'detail']);
    Route::group([
        'prefix' => 'auth'
        ], function ($router) {
    
        Route::get('logout', [AuthController::class, 'logout']); 
     
    });
    
    
});


//
